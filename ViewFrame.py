'''
Created on 23 Jul 2016

@author: kamichal
'''
from PyQt4 import Qt, QtOpenGL
from PyQt4.Qt import QSlider, QToolButton, \
    QHBoxLayout, QGraphicsView, QLabel


class ViewFrame(Qt.QFrame):

    def __init__(self, parent=None):
        super(ViewFrame, self).__init__(parent)
        self.setFrameStyle(Qt.QFrame.Plain | Qt.QFrame.StyledPanel)
        self.graphics_view = GraphicsView(self)
        self.graphics_view.setRenderHint(Qt.QPainter.Antialiasing, False)
        self.graphics_view.setDragMode(QGraphicsView.RubberBandDrag)
        self.graphics_view.setOptimizationFlags(QGraphicsView.DontSavePainterState)
        self.graphics_view.setViewportUpdateMode(QGraphicsView.SmartViewportUpdate)
        self.graphics_view.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)

        size = self.style().pixelMetric(Qt.QStyle.PM_ToolBarIconSize)
        iconSize = Qt.QSize(size, size)

        self.scroll_h_value = 0

        self.zoom_in_button = Qt.QToolButton()
        self.zoom_in_button.setText(u'+')
#         self.zoom_in_button.setIcon(Qt.QPixmap(":/zoomin.png"))
        self.zoom_in_button.setAutoRepeat(True)
        self.zoom_in_button.setAutoRepeatInterval(33)
        self.zoom_in_button.setAutoRepeatDelay(0)
        self.zoom_in_button.setIconSize(iconSize)
        self.zoom_out_button = Qt.QToolButton()
        self.zoom_out_button.setText(u'-')
#         zoom_out_button.setIcon(Qt.QPixmap(":/zoomout.png"))
        self.zoom_out_button.setAutoRepeat(True)
        self.zoom_out_button.setAutoRepeatInterval(33)
        self.zoom_out_button.setAutoRepeatDelay(0)
        self.zoom_out_button.setIconSize(iconSize)
        self.zoom_slider_y = QSlider()
        self.zoom_slider_y.setMinimum(0)
        self.zoom_slider_y.setMaximum(500)
        self.zoom_slider_y.setValue(250)
        self.zoom_slider_y.setTickPosition(QSlider.TicksRight)

        zoom_slider_layout = Qt.QVBoxLayout()
        zoom_slider_layout.addWidget(self.zoom_in_button)
        zoom_slider_layout.addWidget(self.zoom_slider_y)
        zoom_slider_layout.addWidget(self.zoom_out_button)

        self.reset_button = Qt.QToolButton()
        self.reset_button.setText(u'0')
        self.reset_button.setEnabled(False)

        self.label = QLabel(u'name of the view')
        self.label2 = QLabel(u'Pointer Mode')

        self.select_mode_button = QToolButton()
        self.select_mode_button.setText(u'Select')
        self.select_mode_button.setCheckable(True)
        self.select_mode_button.setChecked(True)

        self.drag_mode_button = QToolButton()
        self.drag_mode_button.setText(u'Drag')
        self.drag_mode_button.setCheckable(True)
        self.drag_mode_button.setChecked(False)

        self.antialias_button = QToolButton()
        self.antialias_button.setText(u'Antialiasing')
        self.antialias_button.setCheckable(True)
        self.antialias_button.setChecked(False)

        self.opengl_button = QToolButton()
        self.opengl_button.setText(u'OpenGL')
        self.opengl_button.setCheckable(True)
        self.opengl_button.setEnabled(QtOpenGL.QGLFormat.hasOpenGL())

        self.printButton = QToolButton()
        self.printButton.setText(u'Print')

        pointer_mode_group = Qt.QButtonGroup()
        pointer_mode_group.setExclusive(True)
        pointer_mode_group.addButton(self.select_mode_button)
        pointer_mode_group.addButton(self.drag_mode_button)

        label_layout = QHBoxLayout()
        label_layout.addWidget(self.label)
        label_layout.addStretch()
        label_layout.addWidget(self.label2)
        label_layout.addWidget(self.select_mode_button)
        label_layout.addWidget(self.drag_mode_button)
        label_layout.addStretch()
        label_layout.addWidget(self.antialias_button)
        label_layout.addWidget(self.opengl_button)
        label_layout.addWidget(self.printButton)

        top_layout = Qt.QGridLayout()
        top_layout.addLayout(label_layout, 0, 0)
        top_layout.addWidget(self.graphics_view, 1, 0)
        top_layout.addLayout(zoom_slider_layout, 1, 1)
        top_layout.addWidget(self.reset_button, 2, 1)
        self.setLayout(top_layout)

        self.reset_button.clicked.connect(self.resetView)
        self.reset_button.clicked.connect(self.resetView)

        self.zoom_slider_y.valueChanged.connect(self.setupMatrix)
        self.graphics_view.verticalScrollBar().valueChanged.connect(self.setResetButtonEnabled)
        self.graphics_view.horizontalScrollBar().valueChanged.connect(self.setResetButtonEnabled)
        self.select_mode_button.toggled.connect(self.togglePointerMode)
        self.drag_mode_button.toggled.connect(self.togglePointerMode)
        self.antialias_button.toggled.connect(self.toggleAntialiasing)

        self.opengl_button.toggled.connect(self.toggleOpenGL)
        self.zoom_in_button.clicked.connect(self.zoomIn)
        self.zoom_out_button.clicked.connect(self.zoomOut)
        self.printButton.clicked.connect(self.print_)

        self.setupMatrix()

    def view(self):
        return self.graphics_view

    def resetView(self):
        self.zoom_slider_y.setValue(250)
        self.setupMatrix()
        self.graphics_view.ensureVisible(Qt.QRectF(0, 0, 0, 0))
        self.reset_button.setEnabled(False)

    def setResetButtonEnabled(self):
        self.reset_button.setEnabled(True)

    def togglePointerMode(self):
        if self.select_mode_button.isChecked():
            dragMode = QGraphicsView.RubberBandDrag
        else:
            dragMode = QGraphicsView.ScrollHandDrag
        self.graphics_view.setDragMode(dragMode)
        self.graphics_view.setInteractive(self.select_mode_button.isChecked())

    def toggleOpenGL(self):
        if self.opengl_button.isChecked():
            newOne = QtOpenGL.QGLWidget(QtOpenGL.QGLFormat(QtOpenGL.QGL.SampleBuffers))
        else:
            newOne = Qt.QWidget()
        self.graphics_view.setViewport(newOne)

    def toggleAntialiasing(self):
        doAA = self.antialias_button.isChecked()
        self.graphics_view.setRenderHint(Qt.QPainter.Antialiasing, doAA)

    def print_(self):
        printer = Qt.QPrinter()
        dialog = Qt.QPrintDialog(printer, self)
        if dialog.exec_() == Qt.QDialog.Accepted:
            painter = Qt.QPainter(printer)
            self.graphics_view.render(painter)

    def setupMatrix(self):
        scalex = 2.0 ** ((self.zoom_slider_y.value() - 250) / 50.0)
        scaley = 2.0 ** ((self.zoom_slider_y.value() - 250) / 50.0)
        matrix = Qt.QTransform()
        print self.scroll_h_value
        matrix.translate(self.scroll_h_value / 140.0, 0.0)
        matrix.scale(scalex, scaley)
#         print matrix.m11(), matrix.m12(), matrix.m21(), matrix.m22(), matrix.m31(), matrix.m32()
        self.graphics_view.setTransform(matrix)
        self.graphics_view.translate(self.scroll_h_value / 140.0, 0.0)
        self.setResetButtonEnabled()

    def zoomIn(self, level):
        self.zoom_slider_y.setValue(self.zoom_slider_y.value() + level)

    def zoomOut(self, level):
        self.zoom_slider_y.setValue(self.zoom_slider_y.value() - level)

    def scroll_h(self, amount):
        self.scroll_h_value += amount
        self.setupMatrix()


class GraphicsView(Qt.QGraphicsView):

    def __init__(self, frame, parent=None):
        super(GraphicsView, self).__init__(parent)
        self.frame = frame
        self.setAlignment(Qt.Qt.AlignLeading | Qt.Qt.AlignLeft | Qt.Qt.AlignVCenter)

    def wheelEvent(self, event):
        if event.modifiers() & Qt.Qt.ControlModifier:
            if (event.delta() > 0):
                self.frame.zoomIn(6)
            else:
                self.frame.zoomOut(6)
            event.accept()
        return Qt.QGraphicsView.wheelEvent(self, event)
