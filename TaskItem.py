#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
Created on 21 Jan 2017

@author: kamichal
'''
from PyQt4 import Qt, QtGui, QtCore
from py._builtin import enumerate
import random


class TaskItem(Qt.QGraphicsItem):

    def __init__(self, spec):
        super(TaskItem, self).__init__()

        def get_or_default(nme, default):
            return spec[nme] if nme in spec and spec[nme] else default

        def do_qcolor(tapl):
            if isinstance(tapl, (tuple, list)) \
                    and len(tapl) >= 3 \
                    and all(isinstance(x, (int, float)) for x in tapl):
                return Qt.QColor(*tapl[:3])
            else:
                raise Exception(u'Kolor precyzuj jako listę lub tapla z trzema intami lub flołtami.')

        self.title = get_or_default('title', u'nienazwany')
        self.text = get_or_default('text', u'').split(u'\n')
        self.color = do_qcolor(get_or_default('color', tuple(random_color_with_brightness(123))))
        self.x, self.y = get_or_default('position', (0, 0))
        siz = get_or_default('size', (160, 100))
        self.main_size = Qt.QSizeF(*siz)
        self.w, self.h = siz
        self.setZValue(1)
        self.setAcceptsHoverEvents(True)
        self.setFlags(Qt.QGraphicsItem.ItemIsSelectable |
                      Qt.QGraphicsItem.ItemIsMovable |
                      Qt.QGraphicsItem.ItemIsFocusable)

        self.resizer = TaskResizer(parent=self)
        resizerWidth = self.resizer.rect.width() / 2
        resizerOffset = Qt.QPointF(resizerWidth, resizerWidth)
        self.resizer.setPos(self.boundingRect().bottomRight() - resizerOffset)
        self.resizer.resizeSignal.connect(self.resize)

        self.font_size = 12
        self.setPos(Qt.QPointF(self.x, self.y))

    @QtCore.pyqtSlot(Qt.QPointF)
    def resize(self, change):
        self.main_size += Qt.QSizeF(change.x(), change.y())
        self.prepareGeometryChange()
        self.update()

    def boundingRect(self):
        ''' Needed for refreshing '''
        with_margin = self.main_size + Qt.QSizeF(10, 10)
        return Qt.QRectF(-Qt.QPointF(5, 5), with_margin)

    def paint(self, painter, option, widget):
        if (option.state & QtGui.QStyle.State_Selected):
            fillColor = Qt.QColor(self.color.dark(150))
        else:
            fillColor = Qt.QColor(self.color)

        if (option.state & QtGui.QStyle.State_MouseOver):
            fillColor = fillColor.light(125)

        width = 2 if (option.state & QtGui.QStyle.State_Selected) else 0
        pen = Qt.QPen(Qt.QColor(123, 234, 12))
        pen.setWidth(width)

        lod = option.levelOfDetailFromTransform(painter.worldTransform())

        b = painter.brush()
        painter.setBrush(fillColor)

        main_rect = Qt.QRectF(Qt.QPointF(0, 0), self.main_size)
        if (lod < 0.2):
            painter.fillRect(main_rect, fillColor)
            painter.setBrush(b)
            return

        wysokosc_na_ekranie = self.main_size.height() * lod
        painter.drawRoundedRect(main_rect, 5, 5)
        fs = self.title_font_size(lod)
        wysokosc_czcionki_na_ekranie = fs * lod

        if wysokosc_czcionki_na_ekranie > 7:
            font = Qt.QFont("Times", fs)
            font.setStyleStrategy(Qt.QFont.ForceOutline)
            painter.setFont(font)
            painter.drawText(5, fs + 3, self.title)

            if wysokosc_na_ekranie > 30:
                for i, linia in enumerate(self.text, start=2):
                    y = i * (fs + 3) + 3
                    if y < self.main_size.height():
                        painter.drawText(10, y, linia)

    def title_font_size(self, lod):
        if lod < 0.9:
            return self.font_size / lod
        else:
            return self.font_size


class TaskResizer(Qt.QGraphicsObject):

    resizeSignal = QtCore.pyqtSignal(Qt.QPointF)

    def __init__(self, rect=Qt.QRectF(0, 0, 10, 10), parent=None):
        super(TaskResizer, self).__init__(parent)

        self.setFlag(Qt.QGraphicsItem.ItemIsMovable, True)
        self.setFlag(Qt.QGraphicsItem.ItemIsSelectable, True)
        self.setFlag(Qt.QGraphicsItem.ItemSendsGeometryChanges, True)
        self.rect = rect

    def boundingRect(self):
        return self.rect

    def paint(self, painter, option, widget=None):
        if self.isSelected():
            pen = Qt.QPen(Qt.QColor(240, 120, 50))
            pen.setStyle(Qt.Qt.DotLine)
            painter.setPen(pen)
        painter.drawRoundedRect(self.rect, 3, 3)

    def itemChange(self, change, value):
        if change == Qt.QGraphicsItem.ItemPositionChange:
            if self.isSelected():
                self.resizeSignal.emit(value.toPointF() - self.pos())
        return value


def random_color_with_brightness(brightness):
    if isinstance(brightness, int) and brightness > 1:
        brightness /= 255.0
    saturation = 0.7
    c = Qt.QColor.fromHslF(random.random(), saturation, brightness)
    return c.getRgb()
