#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
Created on 23 Jul 2016

@author: kamichal
Trying to port the example to pyqt:
http://doc.qt.io/qt-4.8/qt-demos-chip-example.html

'''

import sys
from PyQt4 import QtCore, Qt
from ViewFrame import ViewFrame
from Chip import Chip
from TaskItem import TaskItem, random_color_with_brightness
import random
from PyQt4.Qt import QColor

ZADANIA = [
    {
        'title': u'zadańsko 1',
        'text': u'opis tego zadania',
        'size': (200, 90),
    },
    {
        'title': u'zadanie domowe',
        'text': u'to jest zadanie\ndo zrobienia w domu',
        'size': (200, 90),
        'position': (210, 0),
    },
    {
        'title': u'zadanie 2',
        'text': u'to jest mniej wazne\nale zawiera\npodzialy linii',
        'size': (150, 90),
        'position': (410, 0),
    }
]


class OknoGlowne(Qt.QWidget):

    def __init__(self, parent=None):
        super(OknoGlowne, self).__init__(parent)

        self.tabs = Qt.QTabWidget(self)
        self.task_ajtemy = ZADANIA
        tab_1 = Qt.QWidget()
        tab_2 = ViewFrame()
        self.populateScene()
        tab_2.graphics_view.setScene(self.scene)
        edytor_tekstu = Qt.QTextEdit(self)
        edytor_tekstu.setText(self.__opis())
        edytor_tekstu.setReadOnly(True)

        uklad_zakladki_1 = Qt.QVBoxLayout()
        uklad_zakladki_1.addWidget(edytor_tekstu)
        tab_1.setLayout(uklad_zakladki_1)

        uklad_okna = Qt.QVBoxLayout(self)
        uklad_okna.addWidget(self.tabs)

        self.tabs.addTab(tab_1, u"Dane wejściowe")
        self.tabs.addTab(tab_2, u"Rysujjj to")
        self.tabs.setCurrentIndex(1)
        self.tabs.setWindowTitle(u'Pierdyliard czipów')
        self.tabs.show()

    def __opis(self):
        return u''' Okej to jest dobry opis. '''

    def populateScene(self):
        self.scene = Qt.QGraphicsScene()
        for i in xrange(-200, 400, 140):
            for j in xrange(-300, 300, 100):
                if i > -100 and -200 < j < 200:
                    continue
                color = Qt.QColor(*random_color_with_brightness(120))
                item = Chip(color, 0, 0)
                item.setPos(Qt.QPointF(i, j))
                self.scene.addItem(item)

        for task_spec in self.task_ajtemy:
            task = TaskItem(task_spec)
            self.scene.addItem(task)


if __name__ == '__main__':

    app = Qt.QApplication(sys.argv)
    app.setAttribute(QtCore.Qt.AA_DontCreateNativeWidgetSiblings)

    window = OknoGlowne()
    window.resize(666, 333)
    window.show()
    window.setWindowTitle('OknoGlowne')

    sys.exit(app.exec_())
    # or return app.exec()
