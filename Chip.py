'''
Created on 24 Jul 2016

@author: kamichal
'''
from PyQt4 import Qt, QtGui


class Chip(Qt.QGraphicsItem):

    def __init__(self, qcolor, x, y):
        super(Chip, self).__init__()
        self.x = x
        self.y = y
        self.color = qcolor
        self.setZValue((x + y) % 2)
        self.setFlags(Qt.QGraphicsItem.ItemIsSelectable | Qt.QGraphicsItem.ItemIsMovable)
        self.setAcceptsHoverEvents(True)

    def boundingRect(self):
        return Qt.QRectF(0, 0, 110, 70)

    def shape(self):
        path = Qt.QPainterPath()
        path.addRect(14, 14, 82, 42)
        return path

    def paint(self, painter, option, widget):
        if (option.state & QtGui.QStyle.State_Selected):
            fillColor = Qt.QColor(self.color.dark(150))
        else:
            fillColor = Qt.QColor(self.color)

        if (option.state & QtGui.QStyle.State_MouseOver):
            fillColor = fillColor.light(125)

        lod = option.levelOfDetailFromTransform(painter.worldTransform())
        if (lod < 0.2):
            if (lod < 0.125):
                painter.fillRect(Qt.QRectF(0, 0, 110, 70), fillColor)
                return

            b = painter.brush()
            painter.setBrush(fillColor)
            painter.drawRect(13, 13, 97, 57)
            painter.setBrush(b)
            return

        oldPen = painter.pen()
        pen = Qt.QPen(oldPen)
        width = 0
        if (option.state & QtGui.QStyle.State_Selected):
            width += 2

        pen.setWidth(width)
        prev_brush = painter.brush()
        brush = Qt.QBrush(fillColor.dark(120 if option.state & QtGui.QStyle.State_Sunken else 100))
        painter.setBrush(brush)

        painter.drawRect(Qt.QRect(14, 14, 79, 39))
        painter.setBrush(prev_brush)

        if (lod >= 1):
            painter.setPen(Qt.QPen(Qt.Qt.gray, 1))
            painter.drawLine(15, 54, 94, 54)
            painter.drawLine(94, 53, 94, 15)
            painter.setPen(Qt.QPen(Qt.Qt.black, 0))

        if (lod >= 2):
            font = Qt.QFont("Times", 10)
            font.setStyleStrategy(Qt.QFont.ForceOutline)
            painter.setFont(font)
            painter.save()
            painter.scale(0.1, 0.1)
            painter.drawText(170, 180, Qt.QString("Model: VSC-2000 (Very Small Chip) at %1x%2").arg(self.x).arg(self.y))
            painter.drawText(170, 200, Qt.QString("Serial number: DLWR-WEER-123L-ZZ33-SDSJ"))
            painter.drawText(170, 220, Qt.QString("Manufacturer: Chip Manufacturer"))
            painter.restore()

        lines = []
        if (lod >= 0.5):
            for i in xrange(11):
                lines.append(Qt.QLineF(18 + 7 * i, 13, 18 + 7 * i, 5))
                lines.append(Qt.QLineF(18 + 7 * i, 54, 18 + 7 * i, 62))
            for i in xrange(6):
                lines.append(Qt.QLineF(5, 18 + i * 5, 13, 18 + i * 5))
                lines.append(Qt.QLineF(94, 18 + i * 5, 102, 18 + i * 5))

        if (lod >= 0.4):
            lines.append(Qt.QLineF(25, 35, 35, 35))
            lines.append(Qt.QLineF(35, 30, 35, 40))
            lines.append(Qt.QLineF(35, 30, 45, 35))
            lines.append(Qt.QLineF(35, 40, 45, 35))
            lines.append(Qt.QLineF(45, 30, 45, 40))
            lines.append(Qt.QLineF(45, 35, 55, 35))

        painter.drawLines(lines)

#         if len(self.stuff) > 1:
#             p = painter.pen()
#             painter.setPen(Qt.QPen(Qt.Qt.red, 1, Qt.Qt.SolidLine, Qt.Qt.RoundCap, Qt.Qt.RoundJoin))
#             painter.setBrush(Qt.Qt.NoBrush)
#             path = Qt.QPainterPath()
#             path.moveTo(self.stuff.first())
#             for i in xrange(1,len(self.stuff)):
#                 path.lineTo(self.stuff[i])
#             painter.drawPath(path)
#             painter.setPen(p)

    def mousePressEvent(self, event):
        Qt.QGraphicsItem.mousePressEvent(self, event)
        self.update()

    def mouseMoveEvent(self, event):
        if (event.modifiers() & Qt.Qt.ShiftModifier):
            #             self.stuff << event.pos()
            self.update()
            return
        Qt.QGraphicsItem.mouseMoveEvent(self, event)

    def mouseReleaseEvent(self, event):
        Qt.QGraphicsItem.mouseReleaseEvent(self, event)
        self.update()
